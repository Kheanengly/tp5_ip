function display_index1(){

    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const id = urlParams.get('id')
 
    fetch(`https://jsonplaceholder.typicode.com/photos/${id}`).then(async (res) =>{
        let data = await res.json();
        // console.log(data)
        // console.log(data.title)
        // console.log( document.getElementById("display-name"))
        document.getElementById("display-name").innerHTML=data.title;
        document.getElementById("display-id").innerHTML=data.id;
        document.getElementById("image").innerHTML=`<img src="${data.url}" id="return_image" ></img>`;
    })
    
}

display_index1()