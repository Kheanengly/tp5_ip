const value = document.getElementById("browsers");
const bu = document.getElementById("button");
const loading = document.getElementById("loading");
const content_list_un = document.getElementById("content_list_un");
function display(){
    let select = document.getElementById("browsers");
    var value = select.options[select.selectedIndex].value;
    loading.style.display="block";
    content_list_un.style.display="none";
   fetch(`http://universities.hipolabs.com/search?country=${value}`).then(async (res) =>{
        let data = await res.json();
        loading.style.display="none";
        console.log(data);
        let tmp="";
        content_list_un.style.display="block";
        content_list_un.style.display="grid";
        for(let i=0;i<data.length;i++){
            tmp+=
            `
                <div class="list_un" >
                    <div class="list_name_un"><h3>${data[i]["name"]}</h3></div>
                    <div><p class="country_short">(${data[i]["country"]} - ${data[i]["alpha_two_code"]}) </p></div>
                    <div class="list_link_un"><div><i class="fa-solid fa-earth-americas"></i><a href="#">${data[i]["web_pages"]}</a></div></div>
                    
                </div>
            `
        }
         document.getElementById("content_list_un").innerHTML=tmp;  
         tmp=""; 
   })
}

display()