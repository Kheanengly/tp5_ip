const button = document.getElementById("button");
const block3 = document.getElementById("block3");
const name_u = document.getElementById("Name_text");
const gender = document.getElementById("Gender_text");
const probability_text = document.getElementById("Probability_text");
const loading = document.getElementById("loading");


button.addEventListener("click",function(){
   loading.hidden=false;
   block3.hidden=true;
   var name_value = document.getElementById("text_values").value;
   
  
   fetch(`https://api.genderize.io/?name=${name_value}`).then(async (res) =>{
        let data = await res.json();
        loading.hidden=true;
        block3.hidden=false;
        name_u.innerHTML = data["name"];
        gender.innerHTML = data["gender"];
        probability_text.innerHTML = data["probability"]*100 + " %";
   })
});


