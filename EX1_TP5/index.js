const button = document.getElementById("button");
const block3 = document.getElementById("block3");
const activity_text = document.getElementById("activity_text");
const type_text = document.getElementById("type_text");
const participants_text = document.getElementById("participants_text");
const price_text = document.getElementById("price_text");
const Link_text = document.getElementById("Link_text");
const loading = document.getElementById("loading");

button.addEventListener("click",function(){
   loading.hidden=false;
   block3.hidden=true;
   fetch("https://www.boredapi.com/api/activity").then(async (res) =>{
        let data = await res.json();
        loading.hidden=true;
        block3.hidden=false;
        activity_text.innerHTML=data.activity;
        type_text.innerHTML="Type : "+data.type;
        participants_text.innerHTML="Participants : "+data.participants;
        price_text.innerHTML="Price : "+data.price+" $";
        Link_text.innerHTML="Link : "+data.link;
   })
});


