const button = document.getElementById("button");
const block3 = document.getElementById("block3");
const loading = document.getElementById("loading");

// block3.hidden=true;
button.addEventListener("click",function(){
   loading.hidden=false;
   block3.hidden=true;
   var name_value = document.getElementById("text_values").value;
   
  
   fetch(`https://api.nationalize.io?name=${name_value}`).then(async (res) =>{
        let data = await res.json();
        loading.hidden=true;
        block3.hidden=false;
        document.getElementById("c1").innerHTML=data["country"][0].country_id;
        document.getElementById("p1").innerHTML=data["country"][0].probability*100+ " %";
        document.getElementById("p1").style.color="#f06e24";
        document.getElementById("c2").innerHTML=data["country"][1].country_id;
        document.getElementById("p2").innerHTML=data["country"][1].probability*100+ " %";
        document.getElementById("p2").style.color="#f06e24";
        document.getElementById("c3").innerHTML=data["country"][2].country_id;
        document.getElementById("p3").innerHTML=data["country"][2].probability*100+ " %";
        document.getElementById("p3").style.color="#f06e24";
        document.getElementById("c4").innerHTML=data["country"][3].country_id;
        document.getElementById("p4").innerHTML=data["country"][3].probability*100+ " %";
        document.getElementById("p4").style.color="#f06e24";
        document.getElementById("c5").innerHTML=data["country"][4].country_id;
        document.getElementById("p5").innerHTML=data["country"][4].probability*100+ " %";
        document.getElementById("p5").style.color="#f06e24";
        console.log(data.country.probability);
   })
});


